"use strict"; //strict mode is a way to introduce better error-checking into your code 

let tabs = document.querySelector('.tabs');

tabs.addEventListener('click', (ev) => {
    let tabsTitle = document.querySelector('.tabs-title.active');
    tabsTitle.classList.remove('active');
    ev.target.classList.add('active');

    let tabContentTitle = document.querySelector('.tab-content-text.active');
    tabContentTitle.classList.remove('active');
    
    let attribute = ev.target.getAttribute('data-button');
    let activeTabContent = document.querySelector(`.tab-content-text[data-button=${attribute}]`);
    activeTabContent.classList.add('active');
});
